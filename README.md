## Introduction

Module provides custom CSS class settings for field widgets.

## Usage/Setup

* Install module like any other contrib module.
* Once installed, field widget settings can be accessed on
"Manage form display" tab.
* This tab is usually found on the usual entity management screen,
when is comes to fields and display.
* Add css classes as needed.
* Save and inspect new class on field widget.

## Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
